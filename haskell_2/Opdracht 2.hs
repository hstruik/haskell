-- | Harold Struik
-- | 0931946
-- | TI2b
-- | 20/04/2018

module Opdracht2
where
    import Data.List

    data Book = Book{title :: String, author :: String, price :: Int} deriving(Show, Eq)

    instance Ord Book where
      compare a b = compare (title a) (title b)

    data Box a = EmptyBox | Box a deriving (Show)
    data Sack a = EmptySack | Sack a deriving (Show)


    instance Functor Box where
      fmap f EmptyBox = EmptyBox
      fmap f (Box a) = Box (f a)

    instance Functor Sack where
      fmap f EmptySack = EmptySack
      fmap f (Sack a) = Sack (f a)

    bookFromBox (Box a) = a
    fillBox :: [Book] -> Box [Book]
    fillBox a = Box a
    emptyBox :: Box [Book] -> [Book]
    emptyBox a = bookFromBox a

    objectInSack a = Sack (a)
    objectInBox a = Box (a)

    metaList [] = []
    metaList (a:b) = objectInBox (objectInSack a) : metaList b

    numbersBoxList [] = []
    numbersBoxList (a:b) = objectInBox a : numbersBoxList b

    data List a = Empty | Head a (List a) deriving (Show, Read, Eq)

    pushList :: List a -> [a] -> List a
    pushList Empty list = foldr Head Empty list
    pushList (Head h rest) list = foldr Head (Head h rest) list

    changeContainer :: Box a -> Sack a
    changeContainer (Box a) = Sack a

    instance Functor List where
      fmap f Empty = Empty
      fmap f (Head x n) = Head (f x) (fmap f n)
