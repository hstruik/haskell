-- | Harold Struik
-- | 0931946
-- | TI2b
-- | 20/04/2018

import System.IO
import Data.Char

main = do
    file <- getFile
    key <- getKey
    let decryptoText = decrypt file key
    writeFile "Decrypted.txt" decryptoText

getFile = do
    putStrLn "Please enter encrypted file: "
    inp <- getLine >>= readFile
    return inp

getKey = do
    putStrLn "Please enter keyfile: "
    inp <- getLine >>= readFile
    return inp

decrypt :: [Char] -> [Char] -> [Char]
decrypt file key = map (chr . \x -> if x > 127 then x - 128 else x) $ zipWith (-) (fromString file) (toInt key)



toInt :: [Char] -> [Int]
toInt = map ((\x -> if x < 50 then x + 128 else x) . ord)

fromString :: [Char] -> [Int]
fromString [] = []
fromString (x:y:z:xs) = (read [x, y, z]):(fromString xs)
