-- | Harold Struik
-- | 0931946
-- | TI2b
-- | 20/04/2018

import System.IO
import System.Random
import Data.Char

main = do
    file <- getFile
    seed <- getStdGen
    let cryptoKey = fabricateCryptoKey seed file
    let cryptotxt = encrypt file cryptoKey
    writeFile "Encrypted.txt" cryptotxt
    writeFile "cryptoKey.key" cryptoKey

getFile = do
    putStrLn "Please enter your file"
    inp <- getLine >>= readFile
    return inp

fabricateCryptoKey seed file = do
    let randomChars = take 25 (randomRs ('a','z') seed)
    cryptoKey <- take (length file) (cycle randomChars)
    return cryptoKey

encrypt :: [Char] -> [Char] -> [Char]
encrypt file cryptoKey = toString $ zipWith (+) (toInt file) (toInt cryptoKey)

toInt :: [Char] -> [Int]
toInt = map ((\x -> if x < 50 then x + 128 else x) . ord)

toString :: [Int] -> [Char]
toString = concat . map show
