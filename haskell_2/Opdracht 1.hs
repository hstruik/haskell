-- | Harold Struik
-- | 0931946
-- | TI2b
-- | 20/04/2018

        import Data.List
        data Geofig = Square
            {
                length :: Double,
                colour :: Colour
            }
            | Rectangle
            {
                length :: Double,
                width :: Double,
                colour :: Colour
            }
            | Triangle
            {
                length :: Double,
                colour :: Colour
            }
            | Circle
            {
                radius :: Double,
                colour :: Colour
            } deriving (Show)

        data Colour = Red | Blue | Yellow deriving (Eq, Show, Enum)

        data GeoData = GeoData {ar :: Double, circ :: Double, geofig :: Geofig} deriving (Show)

        a=Square 5 Red
        b=Rectangle 2 5 Yellow
        c=Triangle 6 Blue
        d=Circle 3 Red

        area (Square x _) = x*x
        area (Rectangle x y _) = x*y
        area (Triangle x _) = ((x*x)/2)
        area (Circle x _) = (3.14159*x)^2

        circumference (Square x _) = x*4
        circumference (Rectangle x y _) = x+x+y+y
        circumference (Triangle x _) = x+x+x
        circumference (Circle x _) = (2*3.14159*x)

        squarecheck (Square _ _) = True
        squarecheck (Rectangle _ _ _) = False
        squarecheck (Triangle _ _) = False
        squarecheck (Circle _ _) = False

        rectanglecheck (Square _ _) = False
        rectanglecheck (Rectangle _ _ _) = True
        rectanglecheck (Triangle _ _) = False
        rectanglecheck (Circle _ _) = False

        trianglecheck (Square _ _) = False
        trianglecheck (Rectangle _ _ _) = False
        trianglecheck (Triangle _ _) = True
        trianglecheck (Circle _ _) = False

        circlecheck (Square _ _) = False
        circlecheck (Rectangle _ _ _) = False
        circlecheck (Triangle _ _) = False
        circlecheck (Circle _ _) = True

        colourcheck (Square _ c) = c
        colourcheck (Rectangle _ _ c) = c
        colourcheck (Triangle _ c) = c
        colourcheck (Circle _ c) = c

        takesquare :: [Geofig] -> [Geofig]
        takesquare [] = []
        takesquare (h:t) = if squarecheck h == True then h:takesquare t else takesquare t

        takerectangle :: [Geofig] -> [Geofig]
        takerectangle [] =[]
        takerectangle (h:t) = if rectanglecheck h == True then h:takerectangle t else takerectangle t

        taketriangle :: [Geofig] -> [Geofig]
        taketriangle [] = []
        taketriangle (h:t) = if trianglecheck h == True then h:taketriangle t else taketriangle t

        takecircle :: [Geofig] -> [Geofig]
        takecircle [] = []
        takecircle (h:t) = if circlecheck h == True then h:takecircle t else takecircle t

        takegeofig :: String -> [Geofig] -> [Geofig]
        takegeofig geo [] = []
        takegeofig geo (h:t)
            | geo == "Square"    = takesquare (h:t)
            | geo == "Rectangle" = takerectangle (h:t)
            | geo == "Triangle"  = taketriangle (h:t)
            | geo == "Circle"    = takecircle (h:t)
            | otherwise          = []


        takered :: [Geofig] -> [Geofig]
        takered [] = []
        takered (a:b) = if colourcheck a == Red then a:takered b else takered b

        takeblue [] = []
        takeblue (a:b) = if colourcheck a == Blue then a:takeblue b else takeblue b

        takeyellow [] = []
        takeyellow (a:b) = if colourcheck a == Yellow then a:takeyellow b else takeyellow b


        takeColour :: [Geofig] -> Colour -> [Geofig]
        takeColour a b
            | b == Red = takered a
            | b == Blue = takeblue a
            | b == Yellow = takeyellow a


        geoConstruct :: Geofig -> GeoData
        geoConstruct a = GeoData{ar = (area a), circ = (circumference a), geofig = a}


        maxArea :: [GeoData] -> GeoData
        maxArea a = last (sortOn (ar) a)

        maxCirc :: [GeoData] -> GeoData
        maxCirc a = last (sortOn (circ) a)


        addGeofig :: [Geofig] -> Geofig -> [Geofig]
        addGeofig a b = b : a


        totalArea [] = []
        totalArea  (a:b) = ar a : totalArea b


        sumArea :: [GeoData] -> Double
        sumArea a = sum (totalArea a)


        percentageTotal :: GeoData -> Double -> String
        percentageTotal (GeoData {ar = a, circ = b, geofig = c}) d = "Geofig : " ++show c++ " The percentage = " ++show e
            where e = ((a) / (d)) * 100


        areaPercentageSum :: [GeoData ] -> [GeoData] -> [String]
        areaPercentageSum _ [] = []
        areaPercentageSum d (a:b) = c : areaPercentageSum d b
            where c = percentageTotal a (sumArea d)
