module Opd2
    where
        import Data.Char
        -- Opdracht 2

        -- greatest common divisor euclid algorithme
        euclid::Integer -> Integer -> Integer
        euclid x 0 = x
        euclid x y = euclid y (x `mod` y)

        -- extended euclid algorithme
        egcd :: Integer -> Integer -> (Integer,Integer,Integer)
        egcd 0 b = (b, 0, 1)
        egcd a b =
            let (g, s, t) = egcd (b `mod` a) a
             in (g, t - (b `div` a) * s, s)

        -- gets modulus
        getmodulus::Integer -> Integer -> Integer
        getmodulus p q = p * q

        getphi::Integer -> Integer -> Integer
        getphi p q = lcm (p-1) (q-1)

        getlcm::Integer -> Integer -> Integer
        getlcm p q = (p*q `div` (euclid p q)) 

        -- get the e
        gete::Integer -> Integer -> Integer
        gete p q = calce phi counter
            where phi = getphi p q
                  counter = phi - p -- to make it semi random if you dont know the code. -p can be replaced by -1 as well

        calce::Integer -> Integer -> Integer
        calce p q
            | euclid p q == 1 = q
            | otherwise = calce p (q-1)

        -- creates both primary and private key
        getkeypairs::Integer -> Integer -> ((Integer,Integer),(Integer,Integer))
        getkeypairs p q = ((e,n),(d,n))
            where phi = getphi p q
                  n = getmodulus p q
                  e = gete p q
                  d 
                    | b < 0 = b + phi
                    | otherwise = b
                        where (_, b, _) = egcd e phi

        -- returns the public key
        returnpubkey::Integer -> Integer -> (Integer, Integer)
        returnpubkey p q = fst $ getkeypairs p q

        -- returns the private key
        returnprivkey::Integer -> Integer -> (Integer, Integer)
        returnprivkey p q = snd $ getkeypairs p q

        rsaencrypt::(Integer, Integer) -> Integer -> Integer
        rsaencrypt (e, n) x = x ^ e `mod` n

        rsadecrypt::(Integer, Integer) -> Integer -> Integer
        rsadecrypt (d, n) x = x ^ d `mod` n

        encryptchar::(Integer, Integer) -> Char -> Integer
        encryptchar key char = rsaencrypt key asciitointeger
            where asciitointeger = toInteger $ ord char

        decryptchar::(Integer, Integer) -> Integer -> Char
        decryptchar key encryptedchar = integertoascii $ rsadecrypt key encryptedchar
            where integertoascii num = chr $ fromIntegral num