module Opd3
    where
        import Data.List

        p = 1/10000

        differentieer::(Double -> Double) -> Double -> Double -> Double
        differentieer f p x = (f (x + p) - f x) / p

        integreer::(Double -> Double) -> Double -> Double -> Double -> Double
        integreer f a b p = integrate f [a, (a + p)..b]
            where integrate f [] = 0
                  integrate f (head:tail) = p * f head + integrate f tail

        duplicates list = checkstuff list []
            where checkstuff [] returnlist = returnlist
                  checkstuff (head:tail) returnlist 
                    | all check returnlist == True = checkstuff tail $ returnlist ++ [head]
                    | otherwise = checkstuff tail returnlist
                    where check x = x /= head

        dubbelen list = duplicates $ checkstuff list []
            where checkstuff [] returnlist = returnlist
                  checkstuff  (head:tail) returnlist
                    | any check tail == True = checkstuff tail $ returnlist ++ [head]
                    | otherwise = checkstuff tail returnlist
                    where check x = x == head

        dice = [1..6]

        throw = [[one, two, three, four, five]| one <- dice, two <- dice, three <- dice, four <- dice, five <- dice]

        xor''::Bool -> Bool -> Bool
        xor'' True a = not a
        xor'' False a = a

        formatthrow::[Int] -> [(Int, Int)]
        formatthrow throwlist = checkitems throwlist dice []
          where
                checkitems::[Int] -> [Int] -> [(Int, Int)] -> [(Int, Int)] 
                checkitems throwlist [] answer = answer
                checkitems throwlist (h:t) answer = checkitems throwlist t $ answer ++ [(h, countvalue h throwlist)]

        countvalue::Int -> [Int] -> Int
        countvalue value list = length $ filter check list
          where check listvalue = listvalue == value 

        checkpattern::[(Int, Int)] -> [Int] -> Bool
        checkpattern formattedthrow pattern = checkthecheck pattern (ownformat formattedthrow []) True
            where ownformat [] answer = answer
                  ownformat (x:xs) answer = ownformat xs $ answer ++ [snd x]

                  checkthecheck [] checkformat answer = answer
                  checkthecheck (h:t) checkformat answer = checkthecheck t (snd (checkoccurence checkformat [] False)) (answer && (fst (checkoccurence checkformat [] False)))
                      where checkoccurence [] returnlist valuefound = (valuefound, returnlist)
                            checkoccurence (listhead:listtail) returnlist valuefound
                              | (checkvalue listhead) && (valuefound == True) = checkoccurence listtail (returnlist ++ [listhead]) valuefound
                              | (checkvalue listhead) && (valuefound == False) = checkoccurence listtail returnlist True
                              | otherwise = checkoccurence listtail (returnlist ++ [listhead]) valuefound
                                where checkvalue x = x == h

        checkstraightnohomo::[(Int, Int)] -> Bool
        checkstraightnohomo formattedthrow
          | (snd (formattedthrow !! 1)) == 1 && (snd (formattedthrow !! 2)) == 1 && (snd (formattedthrow !! 3)) == 1 && (snd (formattedthrow !! 4)) == 1 && (xor'' ((snd (formattedthrow !! 0)) == 1) ((snd (formattedthrow !! 5)) == 1)) = True
          | otherwise = False
                
        calculate::[[Int]] -> [(String, Float)]
        calculate lotsofthrows = calculate2 (map formatthrow lotsofthrows) [0,0,0,0,0,0,0,0]
          where textValues = ["Poker", "Four of a kind", "Full house", "Three of a kind", "Two Pair", "One Pair", "Straight", "Bust"]
                divlen x = (fromIntegral x) / (fromIntegral (length lotsofthrows))
                calculate2 [] returnlist = zip textValues (map divlen returnlist)
                calculate2 (x:xs) returnlist = calculate2 xs molestx
                  where molestx
                          | checkpattern x [5] = increment 0
                          | checkpattern x [4] = increment 1
                          | checkpattern x [3, 2] = increment 2
                          | checkpattern x [3] = increment 3
                          | checkpattern x [2, 2] = increment 4
                          | checkpattern x [2] = increment 5
                          | checkstraightnohomo x = increment 6
                          | otherwise = increment 7
                        increment listindex = (fst splittedlist) ++ [((head (snd splittedlist)) + 1)] ++ (tail (snd splittedlist))
                          where splittedlist = splitAt listindex returnlist