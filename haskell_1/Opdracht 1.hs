import System.Random

faca :: Int -> Int
faca 0 = 1
faca x = x * faca (x - 1)

facb :: Int -> Int
facb fac
    | fac == 0 = 1
    | fac >  0 = fac*facb(fac-1)

nulpuntena :: Double -> Double -> Double -> [Double]
nulpuntena a b c =
    let discriminant = b*b - 4*a*c
    in (if discriminant < 0 then [] else if discriminant == 0 then [(-b + sqrt(discriminant)) / (2*a)] else [(-b + sqrt(discriminant)) / (2*a),(-b - sqrt(discriminant)) / (2*a)])

nulpuntenb :: Double -> Double -> Double -> [Double]
nulpuntenb a b c
    | discriminant < 0  = error "geen snijpunten met assen"
    | discriminant == 0 = [-b/(2*a)]
    | discriminant > 0  = [-b/(2*a) + (sqrt discriminant)/(2*a),-b/(2*a) - (sqrt discriminant)/(2*a)]
    
    where
    discriminant = b*b - 4*a*c



